

#include <iostream>
using namespace std;

void merge(int arr[], int start, int mid, int end,int &count){
    int* final = new int[end - start + 1];

    int small_index = start;
    int large_index = mid;
    int i = 0;
    while (small_index < mid && large_index <= end) {
        if (arr[small_index] <= arr[large_index]) {
            final[i++] = arr[small_index++];
        }
        else {
            final[i++] = arr[large_index++];
            count = count + mid - small_index;
        }
    }
    while (large_index <= end) {
        final[i++] = arr[large_index++];
    }
    while (small_index < mid) {
        final[i++] = arr[small_index++];
    }
    for (i = 0; i < end - start + 1; i++) {
        arr[start + i] = final[i];
    }
    delete[]final;

    cout << "Sorted Array" << endl;
    for (int i = 0; i <= end; i++) {
        cout << arr[i]<<" ";
    }
    cout << endl;
}

void mergeSort(int arr[], int first, int last,int &count){

    if (first < last){
        int mid = (first + last) / 2;
        mergeSort(arr, first, mid,count);
        mergeSort(arr, mid + 1, last,count);
        merge(arr, first, mid + 1, last,count);
    }

}

int main()
{
    const int size = 5;
    int count = 0;
    int array[size];
    for (int i = 0; i < size; i++) {
        cout << "Enter " << i + 1 << " element: ";
        cin >> array[i];
    }
    mergeSort(array, 0, size - 1,count);
    cout << "count: " << count << endl;
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
