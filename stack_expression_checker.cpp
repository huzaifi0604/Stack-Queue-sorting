// Task 01.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;
template<class a>
struct stacknode {
	a data;
	stacknode* next;
	stacknode() {
		next = NULL;
	}
	stacknode(a ndata) {
		next = NULL;
		data = ndata;
	}
};

template<class a>
class stacklist {
public:
	stacknode<a>* head;
	stacklist() {
		head = NULL;
	}
	void push(a data) {
		if (!head) {
			stacknode<a>* newnode = new stacknode<a>(data);
			newnode->next = NULL;
			head = newnode;
		}
		else {
			stacknode<a>* curr = new stacknode<a>(data);
			curr->next = head;
			head = curr;
		}
	}
	a pop() {
		a value=NULL;
		if (!head) {
			cout << "Sorry! Your Stack is Empty." << endl;
		}
		else {
			stacknode<a>* curr = head;
			head = head->next;
			value = curr->data;
			delete curr;
		}
		return value;
	}
	bool is_empty() {
		if (!head) {
			return true;
		}
		return false;
	}
	void clear() {
		while (!is_empty()) {
			pop();
		}
	}
	void peak() {
		if (!is_empty()) {
			cout << "Sorry! your Stack is Empty." << endl;
		}
		else {
			a value;
			stacknode<a>* curr = head;
			value = curr->data;
			cout << "Peak Value in Stack is " << value << endl;
		}
	}
};
void task2() {
	stacklist<char> sl2;
	char exp[256];
	char val;
	bool var = true;
	cout << "\nEnter your expression to be checked: ";
	cin.get(exp, 256, '\n');
	for (int i = 0; i < 256 || exp[i] != '\0'; i++) {
		if (exp[i] == '(' || exp[i] == '{' || exp[i] == '[') {
			sl2.push(exp[i]);
		}
	}
	for (int i = 0; i < 256 || exp[i] != '\0'; i++) {
		if (exp[i] == ')' || exp[i] == '}' || exp[i] == ']') {
			if (!sl2.is_empty()) {
				if (exp[i] == ')') {
					val = '(';
				}
				else if (exp[i] == '}') {
					val = '{';
				}
				else if (exp[i] == ']') {
					val = '[';
				}
				if (sl2.pop() == val) {
					continue;
				}
				else {
					var = false;
					cout << "Expression is incorrect at " << i << " character\n" << endl;
					break;
				}
			}
			else {
				var = true;
				cout << "Expression is incorrect\nA bracket is missing" << endl;
				break;
			}
		}
	}
	if (!sl2.is_empty()) {
		var = false;
		cout << "Expression is incorrect\nA bracket is missing" << endl;

	}
	if (var == true) {
		cout << "\nCongrats! Your Expression is correct." << endl;
	}
}
void recurse(stacklist<int>& main) {
	if (!main.head->next) {
		cout<<main.pop()<<" ";
		return ;
	}
	else {
		int a=main.pop();
		recurse(main);
		cout << a<<" ";
	}
}
int main()
{
	stacklist<int> sl;
	for (int i = 0; i < 10; i++) {
		sl.push(i);
	}
	sl.peak();
	for (int i = 0; i < 5; i++) {
		int value=sl.pop();
		cout << value << " ";
	}
	cout << endl << endl;
	//sl.clear();
	if (sl.is_empty()) {
		cout << "Stack is Empty\n";
	}
	else {
		cout << "Stack is not Empty.\n";
	}
	cout << sl.is_empty();
	task2();
	stacklist<int> sl3;
	for (int i = 1; i <= 6; i++) {
		sl3.push(i);
	}
	recurse(sl3);
	return 0;


}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
