#include<iostream>
using namespace std;

struct node {
	int data;
	node* next;
};
class queue {
	node* front = NULL;
	node* rear = NULL;
public:
	void enqueue(int ndata) {
		if (!front) {
			node* newnode = new node;
			newnode->data = ndata;
			front = newnode;
			rear = front;
			front->next = NULL;
			rear->next = NULL;
		}
		else {
			node* curr = new node;
			curr->data = ndata;
			rear->next = curr;
			curr->next = NULL;
			rear = curr;
		}
	}
	void frontdata() {
		cout << "Data at front is: " << front->data;
		cout << endl;
	}
	int dequeue() {
		int val=0;
		if (!front) {
			cout << "Sorry! List is Empty." << endl;
		}
		else {
			node* temp = front;
			val = temp->data;
			front = front->next;
			delete temp;

		}
		return val;
	}
	bool is_empty() {
		if (front == NULL) {
			return false;
		}
		return true;
	}
	void is_clear() {
		while (front != NULL) {
			dequeue();
		}
	}
	void display() {
		if (!front) {
			cout << "Sorry! List is Empty" << endl;
		}
		else {
			node* i = front;
			while (i!=rear->next) {
				cout << i->data << " ";
				i = i->next;
			}
		}
	}
};
int main() {
	queue q1;
	for (int i = 0; i < 5; i++) {
		q1.enqueue(i);
	}
	q1.display();
	cout << endl;
	q1.frontdata();
	cout << endl;
	cout<<"removed data: "<<q1.dequeue();
	cout << endl;
	q1.display();
	q1.is_clear();
	cout << endl;
	cout<<q1.is_empty();
	return 0;
}