#include<iostream>
using namespace std;

int split(int arr[], int size) {
	int j = 0;
	int p_point = 0;
	for (int i = 0; i < size; i++) {
		if (arr[i] < p_point) {
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
			j++;
		}
	}
	return j;
}

void arrange(int array[], int size) {
	int part = split(array, size);
	for (int i = 0; part < size && i < part; part++, i += 2) {
		int temp = array[i];
		array[i] = array[part];
		array[part] = temp;
	}
}

/*int main() {
	int arr[] = { 9,-3,5,-2,-8,-6,1,3 };
	int n = *(&arr + 1) - arr;
	arrange(arr, n);
	for (int i = 0; i < n; i++) {
		cout << arr[i] << " ";
	}
	return 0;
}*/